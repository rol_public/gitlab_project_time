# -*- encoding: utf-8 -*-

lib = File.expand_path('../lib/', __FILE__)
$:.unshift lib unless $:.include?(lib)

require 'glpt/version'
require 'date'

Gem::Specification.new do |s|
  s.name        = 'gitlab_project_time'
  s.version     = Glpt::VERSION
  s.date        = DateTime.now.strftime('%Y-%m-%d')
  s.summary     = "gitlab_project_time"
  s.description = "gitlab_project_time"
  s.authors     = ["Roland Laurès"]
  s.email       = 'roland.laures@semifir.com'
  s.files       = Dir.glob("{bin,lib}/**/*") + %w(LICENSE README.md)
  s.homepage    =
    'http://octopuslab.science/'
  s.license       = 'MIT'
  s.executables  = ['glpt']
  s.require_path = 'lib'

  s.add_runtime_dependency 'gitlab', '~> 4.2'
  s.add_runtime_dependency 'thor', '~> 0.20'
end

